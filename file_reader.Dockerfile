FROM python:3.7-stretch

WORKDIR /opt/file_reader/
COPY common common
COPY run_file_reader.py run_file_reader.py
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt &&  mkdir -p /opt/data/ &&  mkdir -p /opt/data/
VOLUME /opt/data/

CMD ["python", "run_file_reader.py" , "--source-folder" , "/opt/data/" , "--nats-uri", "nats://nats:4222" ]
