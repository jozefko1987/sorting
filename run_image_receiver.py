import asyncio
import logging
import time

from common import FileUtils
from common import Communication
from argparse import ArgumentParser

from common.handler_receive_and_process_image import HandlerReceiveAndProcessImage


def init_logger():
    """
    Init logger
    :return: instance of the logger
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    log_formatter = logging.Formatter(fmt=f'%(asctime)s | %(levelname)-4s | %(message)s')
    handler = logging.StreamHandler()
    handler.setFormatter(log_formatter)
    logger.addHandler(handler)
    return logger


def main():
    """
        The main routine.Returns 0 if the program was executed successfully, otherwise -1.
        :return result
    """
    result = True

    parser = ArgumentParser(description='File reader')
    parser.add_argument('--nats-uri', dest='nats_uri', help='NATS uri', required=True)
    args = parser.parse_args()

    # init logger
    logger = init_logger()

    # start execution
    logger.info('######################################################################################')
    logger.info('Image receiver is starting...')
    logger.info('######################################################################################')

    # create event loop
    loop = asyncio.get_event_loop()
    asyncio.set_event_loop(loop)

    handler = HandlerReceiveAndProcessImage(uri=args.nats_uri, logger=logger, loop=loop)
    result = loop.run_until_complete(handler.start())
    if result:
        loop.run_forever()
    else:
        return -1


if __name__ == '__main__':
    main()
