from unittest import TestCase
from common.utils.colors import Colors


class TestFileUtils(TestCase):

    def test_read_color(self):
        with open("images/red.png", 'rb') as f:
            data = f.read()

        data = Colors.get_rgb_from_data(data)
        result = Colors.calc_avg_color(data)
        self.assertEqual(result.red, 255)
        self.assertEqual(result.green, 0)
        self.assertEqual(result.blue, 0)
        color_name = Colors.color_to_text(result)
        self.assertEqual(color_name, "red")

        with open("images/blue.png", 'rb') as f:
            data = f.read()

        data = Colors.get_rgb_from_data(data)
        result = Colors.calc_avg_color(data)
        self.assertEqual(result.red, 0)
        self.assertEqual(result.green, 0)
        self.assertEqual(result.blue, 255)
        color_name = Colors.color_to_text(result)
        self.assertEqual(color_name, "blue")

        with open("images/lightskyblue.png", 'rb') as f:
            data = f.read()

        data = Colors.get_rgb_from_data(data)
        result = Colors.calc_avg_color(data)
        self.assertEqual(result.red, 0x87)
        self.assertEqual(result.green, 0xce)
        self.assertEqual(result.blue, 0xfa)
        color_name = Colors.color_to_text(result)
        self.assertEqual(color_name, "lightskyblue")
