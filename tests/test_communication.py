import asyncio
import time
from unittest import TestCase

from common import Communication

class TestCommunication(TestCase):
    #NATS_URI = "nats://192.168.99.100:4222"
    NATS_URI = "nats://localhost:4222"
    data = []

    @staticmethod
    def get_data() -> Communication.FileData:
        result = Communication.FileData(uuid = 'uuid ', header='header', filename='filename', data='data')
        return result

    async def on_data_callback(self, data: Communication.FileData):
        self.data.append(data)

    def test_read_write_file(self):
        """
        send data, parse them and check result
        """
        self.data.clear()
        loop = asyncio.get_event_loop()
        asyncio.set_event_loop(loop)
        handler = Communication(uri=TestCommunication.NATS_URI, loop=loop, on_data_callback=self.on_data_callback)
        loop.run_until_complete(handler.subscribe_data(subject='abc', queue_name='bcd'))
        loop.run_until_complete(handler.push_data(self.get_data(), subject='abc'))
        time.sleep(3)
        # check result
        excepted_result = self.get_data()
        self.assertEqual(excepted_result.filename, self.data[0].filename)
        self.assertEqual(excepted_result.header, self.data[0].header)
        self.assertEqual(excepted_result.data, self.data[0].data)
        loop.run_until_complete(handler.disconnect())
        time.sleep(3)

    def test_read_write_file_wrong_subject(self):
        """
        send data with wrong subject
        """
        self.data.clear()
        loop = asyncio.get_event_loop()
        asyncio.set_event_loop(loop)
        handler = Communication(uri=TestCommunication.NATS_URI, loop=loop, on_data_callback=self.on_data_callback)
        loop.run_until_complete(handler.subscribe_data(subject='abc', queue_name='bcd'))
        loop.run_until_complete(handler.push_data(self.get_data(), subject='abcd'))
        time.sleep(3)
        # check result
        self.assertEqual(len(self.data), 0)
        loop.run_until_complete(handler.disconnect())
