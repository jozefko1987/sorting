import asyncio
import logging
import sys
from unittest import TestCase, mock
from unittest.mock import patch, mock_open, MagicMock
from common import Communication, FileUtils
from common.handler_sending_images import HandlerSendingImages


class MockCommunication(Communication):
    async def push_data(self, data: Communication.FileData, subject: str = "files") -> bool:
        pass


class TestHandlerSendingImages(TestCase):
    NATS_URI = "nats://localhost:4222"

    @staticmethod
    def create_handler(loop):
        logger = logging.getLogger()
        logger.level = logging.DEBUG
        stream_handler = logging.StreamHandler(sys.stdout)
        logger.addHandler(stream_handler)

        handler = HandlerSendingImages(_nats_uri=TestHandlerSendingImages.NATS_URI, input_folder="tests/images",
                                       loop=loop,
                                       logger=logger)

        return handler

    @patch('common.handler_sending_images.Communication.push_data')
    def test_reading_images(self, communication_mock):
        # create event loop
        loop = asyncio.get_event_loop()
        asyncio.set_event_loop(loop)

        f = asyncio.Future()
        f.set_result(True)
        communication_mock.return_value = f
        handler = TestHandlerSendingImages.create_handler(loop)
        file_list = FileUtils.get_file_list("images/")
        result = loop.run_until_complete(handler.send(file_list))
        self.assertEqual(result, True)
        self.assertEqual(len(file_list), 0)

        f = asyncio.Future()
        f.set_result(False)
        communication_mock.return_value = f
        handler = TestHandlerSendingImages.create_handler(loop)
        file_list = FileUtils.get_file_list("images/")
        file_list_length = len(FileUtils.get_file_list("images/"))
        result = loop.run_until_complete(handler.send(file_list))
        self.assertEqual(result, False)
        self.assertEqual(len(file_list), file_list_length)

