import io
import unittest
from unittest import TestCase, mock
from unittest.mock import  MagicMock

from common import FileUtils
from os.path import isfile, join


class TestFileUtils(TestCase):

    def test_get_files(self):
        result = FileUtils.get_file_list("wrong_folder")
        self.assertIsNone(result)

        result = FileUtils.get_file_list("images/")
        self.assertIsNotNone(result)

        data = FileUtils.read_file(result[0].path + result[0].filename)
        self.assertIsNone(data)

        # append slash
        data = FileUtils.read_file(join(result[0].path, result[0].filename))
        self.assertIsInstance(data, str)

    def test_encode_decode(self):
        result = FileUtils.get_file_list("images/")
        file_name = join(result[0].path, result[0].filename)

        with open(file_name, 'rb') as f:
            data = f.read()
        self.assertIsInstance(data, bytes)
        encoded = FileUtils.encode(data)
        self.assertIsInstance(encoded, str)
        decoded = FileUtils.decode(encoded)
        self.assertIsInstance(decoded, bytes)
        # check if data are equal
        self.assertEqual(data, decoded)

    class Test:
        def write(self, data):
            return 4

    @unittest.mock.patch('builtins.open')
    @unittest.mock.patch('os.path')
    def test_write_file(self, mock_os_path, mock_open):
        data = "abcdefgh"
        mock_os_path.exists.return_value = True
        mock_os_path.join.return_value = 'test'

        mock_write_succ = MagicMock()
        mock_write_succ.write.return_value = 333
        mock_open_result = MagicMock()
        mock_open_result.__enter__ = lambda x: mock_write_succ
        mock_open.return_value = mock_open_result

        result = FileUtils.write_file(file_name='a', folder='b', color='c', data=data)
        self.assertEqual(result, True)

        mock_write_succ = MagicMock()
        mock_write_succ.write.return_value = 0
        mock_open_result = MagicMock()
        mock_open_result.__enter__ = lambda x: mock_write_succ
        mock_open.return_value = mock_open_result

        result = FileUtils.write_file(file_name='a', folder='b', color='c', data=data)
        self.assertEqual(result, False)

