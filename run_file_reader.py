import logging
import asyncio
from argparse import ArgumentParser
from common.handler_sending_images import HandlerSendingImages


def init_logger():
    """
    Init logger
    :return: instance of the logger
    """
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    log_formatter = logging.Formatter(fmt=f'%(asctime)s | %(levelname)-4s | %(message)s')
    handler = logging.StreamHandler()
    handler.setFormatter(log_formatter)
    logger.addHandler(handler)
    return logger


def main():
    """
        The main routine.Returns 0 if the program was executed successfully, otherwise -1.
        :return result
    """
    result = True

    parser = ArgumentParser(description='File reader')
    parser.add_argument('--source-folder', dest='source_folder', help='Source folder', required=True)
    parser.add_argument('--nats-uri', dest='nats_uri', help='NATS uri', required=True)
    args = parser.parse_args()

    # init logger
    logger = init_logger()

    # start execution
    logger.info('######################################################################################')
    logger.info('File reader is starting ...')
    logger.info('######################################################################################')

    # create event loop
    loop = asyncio.get_event_loop()
    asyncio.set_event_loop(loop)

    handler = HandlerSendingImages(_nats_uri=args.nats_uri, input_folder=args.source_folder, loop=loop, logger=logger)

    if result:
        loop.run_until_complete(handler.check_files())


if __name__ == '__main__':
    main()
