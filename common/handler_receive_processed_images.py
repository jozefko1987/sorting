import logging

from common import Communication, FileUtils


class HandlerReceiveProcessedImages:

    def __init__(self, uri: str, output_folder: str, loop, logger: logging.Logger):
        """
        :param uri: connect string
        :param output_folder : output folder
        :param loop: a asyncio loop
        :param logger: instance of logger
        """
        self._uri = uri
        self._output_folder = output_folder
        self._loop = loop
        self._logger = logger or logging.getLogger(__name__)
        self._handler = None

    async def on_data_callback(self, data: Communication.FileData):
        """
        Wait for data and process them
        :param data: callback data
        """
        self._logger.info(f'Got "{data.filename}". Processed color is "{data.header}"')
        self._logger.info(f'Writing to "{self._output_folder}"...')
        result = FileUtils.write_file(file_name=data.filename, folder=self._output_folder, color=data.header,
                                      data=data.data)
        if result:
            self._logger.info('Successfully written!!!')
        else:
            self._logger.info(f'Could not write into "{self._output_folder}"')

    async def start(self):
        """
        Init a communication handler and create a subscription
        :return: if the subscription was created successfully, return True
        """
        self._handler = Communication(uri=self._uri, loop=self._loop, on_data_callback=self.on_data_callback)
        result = await self._handler.subscribe_data(subject='new_changed_file', queue_name='new_changed_file_queue')

        return result
