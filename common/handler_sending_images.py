import logging
import time

from common import Communication, FileUtils


class HandlerSendingImages:

    def __init__(self, _nats_uri: str, input_folder: str, loop, logger: logging.Logger):
        """
        :param uri: connect string
        :param input_folder : input folder path
        :param loop: a asyncio loop
        :param logger: instance of logger
        """
        self._nats_uri = _nats_uri
        self._input_folder = input_folder
        self._loop = loop
        self._logger = logger or logging.getLogger(__name__)
        self._handler = Communication(uri=self._nats_uri, loop=self._loop)

    async def send_data(self, file_list: [FileUtils.FileList]):
        """
           Send data to server
           :param file_list: list of a files read from folder
           :return: False, if a file was not sent
        """
        result = True
        file_list_temp = file_list.copy()
        if file_list:
            while file_list_temp:
                file = file_list_temp.pop()
                file_list.pop()

                result = True
                try:
                    self._logger.info(f'Sending file "{file.filename}"....')
                    data = Communication.FileData(uuid=None, header=None, filename=file.filename,
                                                  data=FileUtils.read_file(file.path + '/' + file.filename))
                    result = await self._handler.push_data(data, subject="new_file")
                except Exception as e:
                    error = str(e)
                    self._logger.error(f'File "{file.filename}" was not sent! {error}')
                    result = False
                    break

                # if error occurred, push it back
                if not result:
                    file_list.append(file)

        return result

    async def send(self, file_list: [FileUtils.FileList]):
        """
        Schedule sending data, async coruotine
        :param file_list: list of a files read from folder
        :return: False, if a file was not sent
        :return: if all files were not sent, return False, otherwise True
        """

        retry_count = 5  # max retries
        retry = 0  # retry counter
        delay = 3.0  # delay in seconds
        while file_list and retry <= retry_count:
            if retry > 0:
                self._logger.warning(f'Retry {retry} from {retry_count}...')
            await self.send_data(file_list)
            if file_list:
                self._logger.warning(f'All files were not sent. Remaining "{len(file_list)}"...')
                retry = retry + 1
                time.sleep(delay)
        if retry >= retry_count:
            return False
        else:
            return True

    async def check_files(self):
        """
        In the endless loop reading content of folder, if is changed resend data
        :return:
        """

        dir_content = ""
        while True:
            dir_content_temp = FileUtils.list_dir(self._input_folder)
            # if content of folder has been changed
            if dir_content_temp != dir_content:
                dir_content = dir_content_temp
                file_list = FileUtils.get_file_list(self._input_folder)
                if file_list:
                    self._logger.info(f'Found {len(file_list)} files....')
                    await self.send(file_list)
                else:
                    self._logger.warning('Could not read a files from the selected folder!')
            # sleep
            time.sleep(5)
