import asyncio
import logging
import time
from common import Communication, FileUtils
from common.utils.colors import Colors


class HandlerReceiveAndProcessImage:

    def __init__(self, uri: str, loop, logger: logging.Logger):
        """
        :param uri: connect string
        :param loop: a asyncio loop
        :param logger: instance of logger
        """
        self._uri = uri
        self._loop = loop
        self._logger = logger or logging.getLogger(__name__)
        self._handler = None
        self._receive_queue = None
        self._control_task = None

    async def on_data_callback(self, data: Communication.FileData):
        """
        Wait for data and process them
        :param data: callback data
        """
        rgb = Colors.get_rgb_from_data(FileUtils.decode(data.data))
        avg = Colors.calc_avg_color(rgb)
        color_name = Colors.color_to_text(avg)
        self._logger.info(f'Got "{data.filename}". Processed color is "{color_name}"')
        result = Communication.FileData(uuid="", header=color_name, filename=data.filename, data=data.data)
        # put into queue
        await self._receive_queue.put(result)

    async def _control(self):
        """
        Receive data from queue and handle them
        """
        try:
            while True:
                data = await self._receive_queue.get()
                self._logger.info(f'Sending changed file "{data.filename}"....')
                result = await self._handler.push_data(data, subject="new_changed_file")
                if not result:
                    time.sleep(5)
                    self._receive_queue.put(data)

        except Exception as e:
            self._logger.error(f'Control task failed. {repr(e)}', exc_info=True)
            # run it again
            self._control_task = self._loop.create_task(self._control())

    async def start(self):
        """
        Init a communication handler and create a subscription
        :return: if the subscription was created successfully, return True
        """
        self._receive_queue = asyncio.Queue()
        self._control_task = self._loop.create_task(self._control())
        self._handler = Communication(uri=self._uri, loop=self._loop, on_data_callback=self.on_data_callback)
        result = await self._handler.subscribe_data(subject='new_file', queue_name='new_file_queue')

        return result
