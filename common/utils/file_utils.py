import base64
import os
from collections import namedtuple


class FileUtils:
    """
    File reader class
    """

    FileList = namedtuple('FileList', 'path filename')

    @staticmethod
    def get_file_list(path: str = None) -> [FileList]:
        """
        :param path: path to folder
        :return: list of files, if folder not exist or wrong parameter is inserted return None
        """
        if path:
            try:
                data = os.listdir(path)
                path = os.path.abspath(path)
                files_only = []
                for i in data:
                    if os.path.isfile(os.path.join(path, i)):
                        files_only.append(i)
                result = [FileUtils.FileList(path=path, filename=i) for i in files_only]
                return result

            except OSError as e:
                return None
        else:
            return None

    @staticmethod
    def decode(data):
        result = base64.b64decode(data)
        return result

    @staticmethod
    def encode(data):
        return base64.b64encode(data).decode("ASCII")

    @staticmethod
    def read_file(path: str = None):
        """
        :param path: path to the file
        :return: read data, otherwise return None
        """
        result = None
        if path:
            try:
                with open(path, 'rb') as f:
                    result = FileUtils.encode(f.read())
                    return result
            except OSError as e:
                result = None
        else:
            result = None

        return result

    @staticmethod
    def write_file(file_name: str, folder: str, color: str, data: str):
        """
        :param file_name: output file name
        :param folder: output folder name
        :param color: color name -> create folder
        :param data: encoded data as str
        :return: if file was written return True, otherwise return False
        """
        result = False
        if file_name and folder and color and data:
            try:
                color_folder = os.path.join(folder, color)
                # if a folder not exist, create it
                if not os.path.exists(color_folder):
                    os.mkdir(color_folder)
                color_file = os.path.join(color_folder, file_name)
                with open(color_file, 'wb') as f:
                    if f.write(FileUtils.decode(data)) > 0:
                        result = True
            except OSError as e:
                result = False

        return result

    @staticmethod
    def list_dir(path: str):
        """
        Read list of directory
        :param path: path
        :return: list directory as string
        """
        return os.listdir(path)
