import asyncio
import json
from collections import namedtuple
from typing import Callable, Any
from nats.aio.client import Client as NATS


class Communication:
    """
    Class for communication via NATS
    """

    FileData = namedtuple('FileData', 'uuid header filename data')

    def __init__(self, uri: str = '"nats://localhost:4222',
                 on_data_callback: Callable[[FileData], Any] = None,
                 loop=asyncio.get_event_loop()):
        """
        :param uri: uri to NATS server
        :param on_data_callback: callback called when new data are received
        :param loop: asycio loop
        """
        self._uri = uri
        self._loop = loop
        self._nc = NATS()
        self._on_data_callback = on_data_callback
        self.sid_list = []

    def get_uri(self):
        """
        :return: NATS server uri
        """
        return self._uri

    async def connect(self):
        """
        Connect to the server
        :return:
        """
        if not self._nc.is_connected:
            await self._nc.connect(self._uri, self._loop, max_reconnect_attempts=5)

        return self._nc.is_connected

    async def disconnect(self):
        """
        Close connection
        :return:
        """
        await self._nc.close()

    async def push_data(self, data: FileData, subject: str = "files") -> bool:
        """
        :param data: named tuple
        :param subject: subject of the message
        :return: True, if the data were successfully sent
        """
        result = await self.connect()
        if result:
            data = {'uuid': data.uuid, 'header': data.header, 'filename': data.filename, 'data': data.data}
            data = json.dumps(data)
            await self._nc.publish(subject, data.encode('ASCII'))
            await self._nc.flush(0.500)
        return result

    async def request_handler(self, msg):
        """
        Transform JSON to named tuple
        :param msg: message
        :return: FileData object
        """
        if self._on_data_callback:
            msg = json.loads(msg.data)
            data = Communication.FileData(uuid=msg['uuid'], header=msg['header'], filename=msg['filename'],
                                          data=msg['data'])
            await self._on_data_callback(data)

    async def subscribe_data(self, subject: str = "files", queue_name: str = "default_queue") -> bool:
        """
        Make subscription
        :param subject: subject of the message
        :param queue_name: subject of the message
        :return: True, if the data were successfully sent
        """
        result = await self.connect()
        if result:
            sid = await self._nc.subscribe(subject, queue_name, cb=self.request_handler)
            if sid:
                self.sid_list.append(sid)
            else:
                result = False

        return result
