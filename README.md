# Sorting machine
### Installation

Install the minikube according to following manual. [link here](https://minikube.sigs.k8s.io/docs/start/)


In GITLAB.com is not able to run CI/CD scripts without inserting a credit card number. You have to build a docker image manually.

If you want to build images directly for the minikube, you have to change an shell's ENV variables.

```minikube -p minikube docker-env``` shows its ENV variables:

_export DOCKER_TLS_VERIFY=”1"
export DOCKER_HOST=”tcp://172.17.0.2:2376"
export DOCKER_CERT_PATH=”/home/user/.minikube/certs”
export MINIKUBE_ACTIVE_DOCKERD=”minikube”_


To point your shell to minikube’s docker-daemon, run:

``` eval $(minikube -p minikube docker-env)```


Build a docker images via folowing commands:


```docker build -t sorting/filereader -f file_reader.Dockerfile . ```

```docker build -t sorting/image-receiver -f image_receiver.DockerFile . ```

```docker build -t sorting/processed-image-receiver -f processed_image_receiver.DockerFile . ```



When a docker images are built and minicube is running start minikube POD's.


Install a NATS server container.

```minikube kubectl --  apply -f https://raw.githubusercontent.com/nats-io/k8s/master/nats-server/single-server-nats.yml```

Create the persistent volume where a result images will be stored.

```minikube kubectl -- create -f kubernetes/volume.yml```


POD's use claims as volumes. The cluster inspects the claim to find the bound volume and mounts that volume for a POD's.

```minikube kubectl -- create -f kubernetes/pv_claim.yml```

The file reader module is reading images from selected folder. If a new image is inserted or deleted resend all images via NATS broker.

```minikube kubectl -- create -f kubernetes/file_reader.yml```

Following module is receiving unprocessed images from NATS server (via queue). Image is processed using CV library and sent to the last module.


```minikube kubectl  -- create -f kubernetes/image_receiver.yaml```\


The last module is receiving processed image.Received data contain an image and WEB color calculated in the previous module. Images are stored in PV of the minikube, ordered in a folders by received WEB color.

```minikube kubectl  -- create -f kubernetes/processed_image_receiver.yaml```


### Usage
Using ***kubectl*** copy images to the 'File Reader' POD.

``` minikube kubectl -- cp tests\images\blue.png file-reader:/opt/data/ ```

Processed images download from 'Processed Image Receiver' POD. This POD using mounted PV storage.

``` minikube kubectl -- cp . processed-image-receiver:/opt/data/ ```

